import UserRepository from "./user.repository";
import ProductRepository from "./product.repository";
import BuyRepository from "./buy.repository";

export { UserRepository, ProductRepository, BuyRepository };
