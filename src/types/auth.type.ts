import { Request } from "express";

export interface IRequest extends Request {
  decoded?: IDecoded;
}

export interface IDecoded {
  id: number;
  email: string;
  isAdm: boolean;
}
