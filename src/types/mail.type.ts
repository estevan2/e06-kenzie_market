export interface ITranporterData {
  host: string;
  port?: number;
  auth: {
    user: string;
    pass: string;
  };
}
