import { getCustomRepository } from "typeorm";
import { Product, User } from "../entities";
import { BuyRepository } from "../repositories";

export default class BuyService {
  static async create(user: User, cart: Product[]) {
    const buyRepository = getCustomRepository(BuyRepository);

    const amount = cart.reduce((a, b) => a + b.price, 0);

    const buy = {
      date: new Date(),
      amount: amount,
    };

    const newBuy = buyRepository.create(buy);
    newBuy.user = user;
    newBuy.cart = cart;
    await buyRepository.save(newBuy);

    return newBuy;
  }

  static async byId(id: number) {
    const buyRepository = getCustomRepository(BuyRepository);

    const buy = await buyRepository.findOne({ id });

    return buy;
  }

  static async byIdWithRelation(id: number) {
    const buyRepository = getCustomRepository(BuyRepository);

    const buy = await buyRepository.findOne({ id }, { relations: ["user", "cart"] });

    return buy;
  }

  static async all() {
    const buyRepository = getCustomRepository(BuyRepository);

    const buy = await buyRepository.find({ relations: ["cart"] });

    return buy;
  }
}
