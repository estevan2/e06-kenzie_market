import { getCustomRepository } from "typeorm";
import { UserRepository } from "../repositories";

export default class CartServices {
  static async byId(id: number) {
    const userRepository = getCustomRepository(UserRepository);

    const cart = await userRepository.findOne({ id }, { relations: ["cart"] });

    return cart;
  }

  static async all() {
    const userRepository = getCustomRepository(UserRepository);

    const carts = await userRepository.find({ relations: ["cart"] });

    return carts;
  }
}
