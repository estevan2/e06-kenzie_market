import { getCustomRepository } from "typeorm";
import { Product } from "../entities";
import { ProductRepository } from "../repositories";

export default class ProductServices {
  static async add(product: Product) {
    const productRepository = getCustomRepository(ProductRepository);

    const newProduct = productRepository.create(product);
    await productRepository.save(newProduct);

    return newProduct;
  }

  static async all() {
    const productRepository = getCustomRepository(ProductRepository);

    const products = await productRepository.find();

    return products;
  }

  static async byId(id: number) {
    const productRepository = getCustomRepository(ProductRepository);

    const product = await productRepository.findOne({ id });

    return product;
  }

  static async byName(name: string) {
    const productRepository = getCustomRepository(ProductRepository);

    const product = await productRepository.findOne({ name });

    return product;
  }
}
