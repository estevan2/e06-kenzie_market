import nodemailer from "nodemailer";
import path from "path";
import hbs from "nodemailer-express-handlebars";
import { MAIL_HOST, MAIL_PASS, MAIL_PORT, MAIL_USER } from "../../config/env.config";
import { ITranporterData } from "../../types/mail.type";

let data: ITranporterData = {
  host: MAIL_HOST as string,
  auth: {
    user: MAIL_USER as string,
    pass: MAIL_PASS as string,
  },
};

if (MAIL_PORT) {
  data.port = Number(MAIL_PORT);
}

const transporter = nodemailer.createTransport(data);

const handlebarsOptions = {
  viewEngine: {
    partialsDir: path.resolve("./src/views/templates/"),
    defaultLayout: undefined,
  },
  viewPath: path.resolve("./src/views/templates/"),
};

transporter.use("compile", hbs(handlebarsOptions));

export default transporter;
