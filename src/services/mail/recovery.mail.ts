import { MAIL_USER } from "../../config/env.config";
import { User } from "../../entities";

const recoveryMailOptionsContructor = (user: User) => {
  return {
    from: MAIL_USER,
    to: user.email,
    subject: "Kenzie Market - Recuperação de Senha",
    template: "recovery",
    context: {
      name: user.name,
      token: user.recovery,
    },
  };
};

export default recoveryMailOptionsContructor;
