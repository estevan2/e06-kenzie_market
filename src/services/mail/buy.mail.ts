import { MAIL_USER } from "../../config/env.config";
import { Buy } from "../../entities";

const buyMailOptionsContructor = (buy: Buy) => {
  let date = buy.date.toLocaleDateString("pt-BR", {
    day: "numeric",
    month: "numeric",
    year: "numeric",
    hour: "numeric",
    minute: "numeric",
    second: "numeric",
  });

  return {
    from: MAIL_USER,
    to: buy.user.email,
    subject: "Kenzie Market",
    template: "buy",
    context: {
      name: buy.user.name,
      buyId: buy.id,
      date: date,
      products: buy.cart,
      amount: buy.amount,
    },
  };
};

export default buyMailOptionsContructor;
