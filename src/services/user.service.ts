import { getCustomRepository } from "typeorm";
import { User } from "../entities";
import { UserRepository } from "../repositories";

export default class UserServices {
  static async create(user: User) {
    const userServices = getCustomRepository(UserRepository);

    const newUser = userServices.create(user);
    await userServices.save(newUser);

    return newUser;
  }

  static async byId(id: number) {
    const userServices = getCustomRepository(UserRepository);

    const user = await userServices.findOne({ id });

    return user;
  }

  static async byEmail(email: string) {
    const userService = getCustomRepository(UserRepository);

    const user = await userService.findOne({ email });

    return user;
  }

  static async all() {
    const userServices = getCustomRepository(UserRepository);

    const usersList = await userServices.find();

    return usersList;
  }

  static async updateUser(user: User) {
    const userServices = getCustomRepository(UserRepository);

    await userServices.save(user);

    return user;
  }
}
