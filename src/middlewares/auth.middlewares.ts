import { NextFunction, Request, Response } from "express";
import { verify } from "jsonwebtoken";
import { SECRET } from "../config/env.config";
import { ResponseError } from "../errors";
import { BuyService } from "../services";
import { IDecoded, IRequest } from "../types";

export const userAuth = (req: IRequest, res: Response, next: NextFunction) => {
  const token = req.headers.authorization?.split(" ")[1];

  if (!token) {
    throw new ResponseError("Missing token authorization.", 401);
  }

  verify(token, SECRET, (error, decoded) => {
    if (error) {
      throw new ResponseError(error.message, 401);
    }
    req.decoded = decoded as IDecoded;
  });

  return next();
};

export const adminAuth = (req: IRequest, res: Response, next: NextFunction) => {
  const user = req.decoded;
  const { id } = req.params;

  if (user?.isAdm) {
    return next();
  }
  if (id && Number(id) === user?.id) {
    return next();
  }

  throw new ResponseError("Missing admin authorization", 401);
};

export const buyOwner = async (req: IRequest, res: Response, next: NextFunction) => {
  const id = req.params.id;
  const user = req.decoded;

  const buy = await BuyService.byIdWithRelation(Number(id));

  if (user?.isAdm) {
    return next();
  }

  if (user?.id === buy?.user.id) {
    return next();
  }

  throw new ResponseError("Missing admin authorization", 401);
};

export const verifyErrorAuth = (error: any, req: IRequest, res: Response, next: NextFunction) => {
  if (error) {
    return res.status(error.statusCode).json({ message: error.message });
  }
};
