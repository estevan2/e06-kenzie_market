import { Express, Router } from "express";
import { BuyControllers } from "../controllers";
import { adminAuth, buyOwner, userAuth, verifyErrorAuth } from "../middlewares";

const router = Router();

const buyRouters = (app: Express) => {
  router.post("", userAuth, verifyErrorAuth, BuyControllers.create);
  router.get("", userAuth, adminAuth, verifyErrorAuth, BuyControllers.all);
  router.get("/:id", userAuth, buyOwner, verifyErrorAuth, BuyControllers.byId);

  app.use("/buy", router);
};

export default buyRouters;
