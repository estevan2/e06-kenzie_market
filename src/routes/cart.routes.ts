import { Router, Express } from "express";
import { CartControllers } from "../controllers";
import { adminAuth, userAuth, verifyErrorAuth } from "../middlewares";

const router = Router();

const cartRouters = (app: Express) => {
  router.get("", userAuth, adminAuth, verifyErrorAuth, CartControllers.all);
  router.get("/:id", userAuth, adminAuth, verifyErrorAuth, CartControllers.byId);
  router.post("", userAuth, verifyErrorAuth, CartControllers.add);
  router.delete("/:product_id", userAuth, verifyErrorAuth, CartControllers.delete);

  app.use("/cart", router);
};

export default cartRouters;
