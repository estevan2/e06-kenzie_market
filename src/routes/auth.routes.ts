import { Router, Express } from "express";
import { UserControllers } from "../controllers";

const router = Router();

const authRouters = (app: Express) => {
  router.post("/login", UserControllers.login);
  // router.post("/email");
  router.post("/recuperar", UserControllers.recovery);
  router.post("/alterar_senha", UserControllers.changePassword);

  app.use(router);
};

export default authRouters;
