import { Router, Express } from "express";
import { UserControllers } from "../controllers";
import { adminAuth, userAuth, verifyErrorAuth } from "../middlewares";

const router = Router();

const userRouters = (app: Express) => {
  router.get("", userAuth, adminAuth, verifyErrorAuth, UserControllers.all);
  router.post("", UserControllers.create);
  router.get("/:id", userAuth, adminAuth, verifyErrorAuth, UserControllers.byId);

  app.use("/user", router);
};

export default userRouters;
