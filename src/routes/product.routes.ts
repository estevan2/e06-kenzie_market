import { Router, Express } from "express";
import { ProductControllers } from "../controllers";
import { adminAuth, userAuth, verifyErrorAuth } from "../middlewares";

const router = Router();

const productRouters = (app: Express) => {
  router.get("", ProductControllers.all);
  router.post("", userAuth, adminAuth, verifyErrorAuth, ProductControllers.create);
  router.get("/:id", ProductControllers.byId);

  app.use("/product", router);
};

export default productRouters;
