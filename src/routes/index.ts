import userRouters from "./user.routes";
import productRouters from "./product.routes";
import authRouters from "./auth.routes";
import cartRouters from "./cart.routes";
import buyRouters from "./buy.routes";

export { userRouters, productRouters, authRouters, cartRouters, buyRouters };
