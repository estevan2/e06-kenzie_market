import { Request, Response } from "express";
import { CartServices, ProductServices, UserServices } from "../services";
import { IRequest } from "../types";

export default class CartControllers {
  static async all(req: IRequest, res: Response) {
    try {
      const users = await CartServices.all();

      const cartsWithId = users.map((user) => {
        const cart = { id: user.id, cart: user.cart };

        return cart;
      });

      return res.json(cartsWithId);
    } catch (e: any) {
      let message = e.message.replace(/\"/, "");
      return res.status(400).json({ message });
    }
  }

  static async byId(req: IRequest, res: Response) {
    try {
      const { id } = req.params;
      const user = await CartServices.byId(Number(id));

      if (!user) {
        throw new Error("user not found.");
      }

      res.json(user.cart);
    } catch (e: any) {
      res.status(400).json({ message: e.message });
    }
  }

  static async add(req: IRequest, res: Response) {
    try {
      const { productName } = req.body;
      const userId = req.decoded?.id;

      const product = await ProductServices.byName(productName);
      const user = await CartServices.byId(Number(userId));

      if (!user) {
        throw new Error("user not found.");
      }

      if (!product) {
        throw new Error("product not found.");
      }
      user.cart.push(product);

      const userUpdated = await UserServices.updateUser(user);

      res.json({ cart: userUpdated.cart });
    } catch (e: any) {
      res.status(400).json({ message: e.message });
    }
  }

  static async delete(req: IRequest, res: Response) {
    try {
      const { product_id } = req.params;
      const userId = req.decoded?.id;

      const user = await CartServices.byId(Number(userId));

      if (!user) {
        throw new Error("User not Found");
      }

      user.cart = user.cart.filter((product) => product.id !== Number(product_id));
      const userUpdated = await UserServices.updateUser(user);

      res.json({ cart: userUpdated.cart });
    } catch (e: any) {
      res.status(400).json({ message: e.message });
    }
  }
}
