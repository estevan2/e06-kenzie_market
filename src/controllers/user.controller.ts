import { hash, compare, hashSync } from "bcryptjs";
import { Request, Response } from "express";
import { UserServices } from "../services";
import { JsonWebTokenError, sign, verify } from "jsonwebtoken";
import { EXPIRE_IN, SECRET } from "../config/env.config";
import { IRequest } from "../types";
import { recoveryMailOptionsContructor, transporter } from "../services/mail";
import SMTPTransport from "nodemailer/lib/smtp-transport";

export default class UserControllers {
  static async create(req: IRequest, res: Response) {
    try {
      const user = req.body;

      user.password = await hash(user.password, 10);

      user.isAdm = !!user.isAdm;

      const userCreated = await UserServices.create(user);
      const { password, ...userWithoutPassword } = userCreated;

      return res.status(201).json(userWithoutPassword);
    } catch (e: any) {
      let message = e.message.replace(/\"/, "");
      return res.status(400).json({ message });
    }
  }

  static async all(req: IRequest, res: Response) {
    try {
      const listUsers = await UserServices.all();

      const listWithoutPasswprd = listUsers.map((user) => {
        const { password, ...userWithoutPassword } = user;
        return userWithoutPassword;
      });

      return res.json(listWithoutPasswprd);
    } catch (e: any) {
      let message = e.message.replace(/\"/, "");
      return res.status(400).json({ message });
    }
  }

  static async byId(req: IRequest, res: Response) {
    try {
      const { id } = req.params;

      const user = await UserServices.byId(Number(id));

      if (!user) {
        throw new Error("user not found.");
      }

      const { password, ...userWithoutPassword } = user;

      return res.json(userWithoutPassword);
    } catch (e: any) {
      res.status(404).json({ message: e.message });
    }
  }

  static async login(req: IRequest, res: Response) {
    try {
      const { email, password } = req.body;

      const user = await UserServices.byEmail(email);

      if (!user) {
        throw new Error("email/password incorrect");
      }

      if (!(await compare(password, user.password))) {
        throw new Error("email/passwordç incorrect");
      }

      const forToken = {
        id: user.id,
        email: user.email,
        isAdm: user.isAdm,
      };

      const token = sign(forToken, SECRET, { expiresIn: EXPIRE_IN });

      res.json({ token: token, user: forToken });
    } catch (e: any) {
      res.status(400).json({ message: e.message });
    }
  }

  static async recovery(req: IRequest, res: Response) {
    try {
      const { email } = req.body;

      if (!email) {
        throw new Error("email expected");
      }

      const user = await UserServices.byEmail(email);

      if (!user) {
        throw new Error("User not found.");
      }

      const forToken = {
        id: user.id,
        name: user.name,
        email: user.email,
      };

      const recoveryToken = sign(forToken, SECRET, { expiresIn: "1h" });

      user.recovery = recoveryToken;

      await UserServices.updateUser(user);

      const mailOptions = recoveryMailOptionsContructor(user);

      let mailInfo: SMTPTransport.SentMessageInfo | undefined;

      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          throw new Error(error.message);
        }
        mailInfo = info;
      });

      return res.json({ message: "Recovery token sent to your email.", info: mailInfo });
    } catch (e: any) {
      res.status(404).json({ message: e.message });
    }
  }

  static async changePassword(req: IRequest, res: Response) {
    try {
      const { recoveryCode, password } = req.body;

      let decoded!: any;

      verify(recoveryCode, SECRET, (error: any, decode: any) => {
        if (error) {
          throw new Error(error.message);
        }
        decoded = decode;
      });

      const user = await UserServices.byEmail(decoded.email);

      if (!user) {
        throw new Error("User not found.");
      }

      if (recoveryCode !== user.recovery) {
        throw new Error("Recovery code invalid.");
      }

      user.password = hashSync(password, 10);

      user.recovery = undefined;

      await UserServices.updateUser(user);

      return res.json({ message: "Password updated." });
    } catch (e: any) {
      return res.status(400).json({ message: e.message });
    }
  }
}
