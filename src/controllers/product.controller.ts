import { Request, Response } from "express";
import { ProductServices } from "../services";
import { IRequest } from "../types";

export default class ProductControllers {
  static async all(req: IRequest, res: Response) {
    try {
      const products = await ProductServices.all();

      res.json(products);
    } catch (e: any) {
      res.status(400).json({ message: e.message });
    }
  }

  static async create(req: IRequest, res: Response) {
    try {
      const product = req.body;

      const productCreated = await ProductServices.add(product);

      res.json(productCreated);
    } catch (e: any) {
      res.status(400).json({ message: e.message });
    }
  }

  static async byId(req: IRequest, res: Response) {
    try {
      const { id } = req.params;

      const product = await ProductServices.byId(Number(id));

      if (!product) {
        throw new Error("product not found.");
      }

      res.json(product);
    } catch (e: any) {
      res.status(404).json({ message: e.message });
    }
  }
}
