import { Request, Response } from "express";
import { BuyService, CartServices, UserServices } from "../services";
import { buyMailOptionsContructor, transporter } from "../services/mail";
import { IRequest } from "../types";

export default class BuyControllers {
  static async create(req: IRequest, res: Response) {
    try {
      const userId = req.decoded?.id as number;

      const userWithCart = await CartServices.byId(userId);

      if (!userWithCart) {
        throw new Error("User not found.");
      }

      if (userWithCart.cart.length === 0) {
        throw new Error("Missing product in cart.");
      }

      const buy = await BuyService.create(userWithCart, userWithCart.cart);

      const mailOptions = buyMailOptionsContructor(buy);

      let mailInfo: any;

      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          throw new Error(error.message);
        }
        mailInfo = info;
      });

      userWithCart.cart = [];

      await UserServices.updateUser(userWithCart);

      const { user, ...buyWithoutUser } = buy;

      return res.json({ info: mailInfo, buy: buyWithoutUser });
    } catch (e: any) {
      return res.status(400).json({ message: e.message });
    }
  }

  static async byId(req: IRequest, res: Response) {
    try {
      const { id } = req.params;

      const buy = await BuyService.byIdWithRelation(Number(id));

      if (!buy) {
        throw new Error("Buy not found.");
      }

      const { user, ...buyWithoutUser } = buy;

      return res.json({ buy: buyWithoutUser });
    } catch (e: any) {
      return res.status(404).json({ message: e.message });
    }
  }

  static async all(req: IRequest, res: Response) {
    try {
      const buy = await BuyService.all();

      return res.json(buy);
    } catch (e: any) {
      return res.status(400).json({ message: e.message });
    }
  }
}
