import express from "express";
import swaggerUiExpress from "swagger-ui-express";
import { logger } from "./middlewares";
import { authRouters, buyRouters, cartRouters, productRouters, userRouters } from "./routes";
import swaggerDocument from "./swagger.json";

const app = express();

app.use(express.json());
app.use(logger);

userRouters(app);
productRouters(app);
cartRouters(app);
authRouters(app);
buyRouters(app);

app.use("/", swaggerUiExpress.serve, swaggerUiExpress.setup(swaggerDocument));

export default app;
