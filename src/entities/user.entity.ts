import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Buy, Product } from ".";

@Entity()
export default class User {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  name!: string;

  @Column({ unique: true })
  email!: string;

  @Column()
  password!: string;

  @Column({ nullable: true })
  recovery?: string;

  @Column()
  readonly isAdm!: boolean;

  @OneToMany((type) => Buy, (buy) => buy.user)
  buy!: Buy[];

  @ManyToMany((type) => Product, { cascade: true })
  @JoinTable()
  cart!: Product[];
}
