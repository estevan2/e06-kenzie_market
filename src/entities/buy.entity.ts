import { Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Product, User } from ".";

@Entity()
export default class Buy {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  date!: Date;

  @Column({ type: "float" })
  amount!: number;

  @ManyToOne((type) => User, (user) => user.buy)
  user!: User;

  @ManyToMany((type) => Product, { cascade: true })
  @JoinTable()
  cart!: Product[];
}
